package com.zp4rker.jstick;

import com.zp4rker.jstick.cmd.JailStickCMD;
import com.zp4rker.jstick.lstnr.EntityDamageByEntity;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class JailStick extends JavaPlugin {

    public void onEnable() {

        getServer().getPluginManager().registerEvents(new EntityDamageByEntity(this), this);

        getCommand("jailstick").setExecutor(new JailStickCMD(this));

        saveDefaultConfig();

    }

    public ItemStack getStickItem() {
        String itemType = getConfig().getString("item-type");
        String displayName = getConfig().getString("display-name");
        List<String> lore = getConfig().getStringList("lore");
        ItemStack itemStack = new ItemStack(Material.valueOf(itemType));
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public String getCommand() {
        return getConfig().getString("command");
    }

    public int getHitAmount() {
        return getConfig().getInt("hit-amount");
    }

    public String translatePHolders(String s, Player player, Player attacker, int amount) {
        return s.replace("%player%", player.getName())
        .replace("%attacker%", attacker.getName())
        .replace("%amount", "" + amount)
        .replace("%hitsleft%", "" + (getHitAmount() - amount));
    }

}