package com.zp4rker.jstick.lstnr;

import com.zp4rker.jstick.JailStick;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.UUID;

public class EntityDamageByEntity implements Listener {
    private JailStick plugin;
    private static HashMap<UUID, Integer> tasks = new HashMap<>();
    private static HashMap<UUID, Integer> hits = new HashMap<>();

    public EntityDamageByEntity(JailStick plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent event) {

        if (event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
            Player damager = (Player) event.getDamager();
            Player player = (Player) event.getEntity();
            if (compareItems(damager.getInventory().getItemInMainHand(), plugin.getStickItem())) {
                if (hits.containsKey(player.getUniqueId())) {
                    int currentHits = hits.get(player.getUniqueId());
                    currentHits++;
                    player.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("hit-player-msg"),
                            player, damager, currentHits));
                    damager.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("hit-attacker-msg"),
                            player, damager, currentHits));
                    if (currentHits == plugin.getHitAmount()) {
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(),
                                plugin.translatePHolders(plugin.getCommand(), player, damager, currentHits));
                        plugin.getServer().getScheduler().cancelTask(tasks.get(player.getUniqueId()));
                        hits.remove(player.getUniqueId());
                        tasks.remove(player.getUniqueId());
                        if (plugin.getConfig().getBoolean("at-hit-amount.both-msg")) {
                            player.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("hit-player-msg"),
                                    player, damager, currentHits));
                            damager.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("hit-attacker-msg"),
                                    player, damager, currentHits));
                        }
                        player.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("at-hit-amount.player-msg"),
                                player, damager, currentHits));
                        damager.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("at-hit-amount.attacker-msg"),
                                player, damager, currentHits));
                    } else {
                        hits.replace(player.getUniqueId(), currentHits);
                    }
                } else {
                    hits.put(player.getUniqueId(), 1);
                    player.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("hit-player-msg"),
                            player, damager, 1));
                    damager.sendMessage(plugin.translatePHolders(plugin.getConfig().getString("hit-attacker-msg"),
                            player, damager, 1));
                    tasks.put(player.getUniqueId(),
                            plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                            @Override
                            public void run() {
                                hits.remove(player.getUniqueId());
                                plugin.getServer().getScheduler().cancelTask(tasks.get(player.getUniqueId()));
                                tasks.remove(player.getUniqueId());
                                // Add messages here
                            }
                        },
                                    20 * plugin.getConfig().getInt("hit-period")));
                }
            }
        }

    }

    private boolean compareItems(ItemStack one, ItemStack two) {
        try {
            return (ChatColor.stripColor(one.getItemMeta().getDisplayName()).equalsIgnoreCase(
                    ChatColor.stripColor(two.getItemMeta().getDisplayName())));
        } catch (NullPointerException e) {
            return false;
        }
    }

}