package com.zp4rker.jstick.cmd;

import com.zp4rker.jstick.JailStick;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JailStickCMD implements CommandExecutor {
    private JailStick plugin;

    public JailStickCMD(JailStick plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (cmd.getLabel().equalsIgnoreCase("jailstick")) {
            if (sender instanceof Player) {
                if (sender.hasPermission("jailstick.use")) {
                    ((Player) sender).getInventory().addItem(plugin.getStickItem());
                    sender.sendMessage(plugin.getConfig().getString("receive-msg"));
                    return true;
                }
            } else {
                sender.sendMessage(plugin.getConfig().getString("not-player-msg"));
                return true;
            }
        }
        return false;
    }

}
